const express= require('express')
const router=express.Router()
const fetch = require('node-fetch') // *npm i node-fetch*/
const apiKey= '28e40b98'
const baseURL=`http://www.omdbapi.com/?apiKey=${apiKey}`
const redirect= `http://www.omdbapi.com/`


router.route('/search')
.get((req, res)=>res.render('search'))
.post((req, res)=>{ 
    const busqueda = req.body.busqueda
    fetch(`${baseURL}&s=${busqueda}`)
    .then(data=>data.json())

    .then(data=>res.render('results', {resultados: data.Search})
)
})  


router.get('/detalle-pelicula/id:',(req,res)=>{
    const idPeli = req.params.id;
    fetch(`${baseURL}&i{idPeli}`)
    .then(data=>res.render('results', {resultados: data.idPeli})
    .then(data=>res.render())
   )
})





router.get('*',(req, res)=>
 {
     
    
res.render('error', {
texto: '404 Page not Found'
});

}
)

// router.get('*',(req,res)=>res.send('404'))
module.exports = router







