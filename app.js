

const express = require('express');
const app = express();
//const router=express.Router();
const path= require('path');
const port = 3000;



app.set('view engine', 'pug')
app.set('views','views')//Primer campo es el nombre de la variable y la seguna es la ruta en la que estamos(o lo que queremos utilizar)
app.use (express.urlencoded({extended:false}))


//Aqui van mis ROUTERS
const rutasBasicas = require('./routes/rutasBasicas')

app.use(express.static('public'))/*Todas las rutas estaticas las buscarÃ¡ aqui */
app.use(rutasBasicas)
// app.use('/', (req, res, next)=>{
//     console.log('Hola desde express')
//     next();
// })

/*aqui ponemos las rutas, con (path.join) metemos las rutas 
y nombres de los ficheros y archivos que queremos mostrar  

app.use('/views/styles.css', (req, res)=>res.sendFile(path.join(__dirname, "views","styles.css")))*/

/*Esta linea pintara la ruta (/hola) pero hay que cambiar 
la posicion encima de la otra ruta (/), sino no nos lo muestra 

app.use('/hola', (req, res)=>res.send('<h1>Hola desde Express desde otra URL</h1>'))*/


app.listen(port,() =>console.log('Servidor Corriendo'))
 /*Con este metodo listen() hacemos que cada vez que levantemos el servidor
 al puerto indicado en la constante de arriba, nos imprimira una frase por 
 consola de servidor corriendo, la ponemos al final del documento*/



